import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule, OnInit } from '@angular/core';
import { FormsModule } from '@angular/forms';
// import { HttpModule } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';
import { Router, NavigationStart, NavigationEnd, NavigationError, NavigationCancel, RoutesRecognized, RouteConfigLoadStart, RouteConfigLoadEnd } from '@angular/router';
import { FlexLayoutModule } from '@angular/flex-layout';
import 'hammerjs';

import { CoreModule } from './core/core.module';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { StorageService } from './core/services/storage.service';
import { TranslationService } from './core/services/translation.service';
import { ITranslation } from './core/core.interfaces';

// Imports for loading & configuring the in-memory web api
// import { InMemoryWebApiModule } from 'angular-in-memory-web-api';
// import { LanguageData } from '../data/language.data';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    //HttpModule,
    HttpClientModule,
    FlexLayoutModule,
    CoreModule.forRoot(),
    AppRoutingModule,
    // InMemoryWebApiModule.forRoot(LanguageData, { delay: 1000 })
  ],
  exports: [
  ],
  providers: [
  ],
  bootstrap: [
    AppComponent
  ]
})
export class AppModule {

  // Error state
  errorMessage: string;

  constructor(private router: Router, private browserStorage: StorageService, private translationService: TranslationService) {

    // TODO :: Move this code out of constructor to the separate method.
    const NAVIGATOR_LANGUAGE = navigator.language;
    const CURRENTLY_SELECTED_LANGUAGE = this.browserStorage.localStorageRetriveItem('languageCode');

    // Set language if no record with key languageCode was found in browser storage.
    // Otherwise do not update the value.
    if (CURRENTLY_SELECTED_LANGUAGE === null) {
      this.browserStorage.localStorageSaveItem('languageCode', NAVIGATOR_LANGUAGE);
    }

    // Get locale translation
    // Get localization according to selected language
    const REQUEST_LOCALE = CURRENTLY_SELECTED_LANGUAGE === null ? NAVIGATOR_LANGUAGE : CURRENTLY_SELECTED_LANGUAGE;

    // Trigger translation service initial read
    this.translationService.getLocaleTranslation(REQUEST_LOCALE);
  }
}
