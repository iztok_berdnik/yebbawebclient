
// APPLICATION LOCAL STORAGE
export interface ILocalStorage {
  token: string;
  languageCode: string;
}

// APPLICATION ENVIRONMENT PROPERTIES
export interface IApplicationEnvironment {
  developmentMode: boolean;
  build: string;
  apiURL: string;
  browser: string;
}

/* LANGUAGE */
export interface ILanguage {
  id: number;
  code: string;
  native: string;
  global: string;
}

/* TRANSLATION */
export interface ITranslation {
  id: number;
  value: string;
}

/* GEOLOCATION */
export interface IGeolocation {
  latitude: number;
  longitude: number;
}

/* RESET PASSWORD */
export interface IResetPassword {
  password: string;
  passwordConfirm: string;
}
