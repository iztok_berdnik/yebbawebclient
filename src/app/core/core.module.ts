import { NgModule, Optional, SkipSelf, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';

import { EnvironmentService } from './services/environment.service';
import { TranslationService } from './services/translation.service';
import { StorageService } from './services/storage.service';
import { GeolocationService } from './services/geolocation.service';
import { HelperService } from './services/helper.service';

// Array of core services that are available in accros entire application.
// There is no need to re-import any of this services anywhere in application.
/*
export const CORE_SERVICES_LIST = [
  EnvironmentService,
  TranslationService
]
*/

@NgModule({
  id: 'M_CORE',
  imports: [
    CommonModule
  ],
  exports: [
  ],
  declarations: [],
  providers: [
    // !!! IMPORTANT !!! Do not provide any service this was. It is handled by forRoot method of CoreModule.
  ]
})

export class CoreModule {

  constructor( @Optional() @SkipSelf() parentModule: CoreModule) {
    // Prevent import of core module in any module other then AppModule
    if (parentModule) {
      throw new Error('CoreModule is already loaded. Import it in the AppModule only');
    }
  }

  static forRoot(): ModuleWithProviders {
    return {
      ngModule: CoreModule,
      providers: [
        EnvironmentService,
        TranslationService,
        StorageService,
        GeolocationService,
        HelperService
      ]
    }
  }
}
