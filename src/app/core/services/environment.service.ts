import { Injectable } from '@angular/core';

import { ENVIRONMENT } from '../../../environments/environment';
import { IApplicationEnvironment } from '../core.interfaces';

@Injectable()
export class EnvironmentService {

  constructor() { }

  // Public method for retrieving client environment properties.
  getEnvironmentProperties() {
    const _isDevelopmentMode = this.resolveEnvironment();
    const _buildMode = this.resolveBuild();
    const _apiURI = this.resolveHost();
    const _browser = this.resolveBrowser();

    const CLIENT_ENVIRONMENT: IApplicationEnvironment = {
      'developmentMode': _isDevelopmentMode,
      'build': _buildMode,
      'apiURL': _apiURI,
      'browser': _browser
    }

    return CLIENT_ENVIRONMENT;
  }

  // Private method for resolving mode in which client is running. Rule is that if it runs on port 4250 then
  // it is running in development mode, otherwise it is quite safe to assume that it is running in development mode.
  private resolveEnvironment() {
    const _IS_DEVELOPMENT_MODE = window.location.port.indexOf('4250') > -1 ? true : false ;

    console.log(':: _IS_DEVELOPMENT_MODE :: ', _IS_DEVELOPMENT_MODE);
    return _IS_DEVELOPMENT_MODE;
  }

  // Private method for resolving mode in which client is build. It is either production or development mode.
  // Later this could be expanded to other modes (e.g. staging), but for now it is all there is needed.
  private resolveBuild() {
    const _isProductionBuild = ENVIRONMENT.production === true ? 'PRODUCTION' : 'DEVELOPMENT';

    return _isProductionBuild;
  }

  // Private method for resolving protocol and host of the client. It is returning value like https://www.google.si
  // or http://localhost:4250
  private resolveHost() {
    const _PROTOCOL = window.location.protocol;
    const _HOSTNAME = window.location.hostname;
    const _HOST = window.location.host;
    const _PORT = _HOST === 'localhost:4250' ? ':' + 3005 : window.location.port;

    return _PROTOCOL + '//' + _HOSTNAME + _PORT;
  }

  // Method for detecting browser.
  private resolveBrowser() {
    // Check if browser is IE
    if (navigator.userAgent.search('MSIE') >= 0) {
      // insert conditional IE code here
      return 'Internet Explorer/Edge';
    }
    //Check if browser is Chrome
    else if (navigator.userAgent.search('Chrome') >= 0) {
      // insert conditional Chrome code here
      return 'Chrome';
    }
    // Check if browser is Firefox
    else if (navigator.userAgent.search('Firefox') >= 0) {
      // insert conditional Firefox Code here
      return 'Firefox';
    }
    // Check if browser is Safari
    else if (navigator.userAgent.search('Safari') >= 0 && navigator.userAgent.search('Chrome') < 0) {
      // insert conditional Safari code here
      return 'Safari';
    }
    //Check if browser is Opera
    else if (navigator.userAgent.search('Opera') >= 0) {
      // insert conditional Opera code here
      return 'Opera';
    }
    else {
      return 'Unknown browser';
    }
  }
}
