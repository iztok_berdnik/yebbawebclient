import { Injectable } from '@angular/core';

@Injectable()
export class HelperService {

  constructor() { }


  // Method responsible for iterating through array of objects and returns arrays that match key
  // In parms:
  //  [0]   ::  Array that should be searched
  //  [1]   ::  Key (string) that is beeing searched
  //  [2]   ::  Property to find it
  // Output:
  //   => item
  public findItemInArray(array: any, key: string, prop: any) {
    // Optional, but fallback to key['name'] if not selected
    prop = (typeof prop === 'undefined') ? '' : prop;

    // Loop throw object and return first record that matches required filter
    for (var i = 0; i < array.length; i++) {
      if (array[i][key] === prop) {
        return array[i];
      }
    }
  }

  // Method responsible for comparing to strings and returning result
  // In parms:
  //  [0]  ::  firstString
  //  [1]  ::  secondString
  // Output:
  //  => true or false
  public stringMatcher(firstString: string, secondString: string) {
  // console.log(c);
  return firstString === secondString ? true : false;
}

}
