import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
// import { Subject } from 'rxjs/Subject';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import 'rxjs/add/operator/retry';

import { EnvironmentService } from '../services/environment.service';
import { IApplicationEnvironment, ILanguage } from '../core.interfaces';

const DEFAULT_LANGUAGES_LIST: ILanguage[] = [
    {
    "id": 1,
    "code": "en-US",
    "native": "English",
    "global": "English"
    },
    {
    "id": 2,
    "code": "sl-SI",
    "native": "Slovenščina",
    "global": "Slovenian"
    },
    {
    "id": 3,
    "code": "de-DE",
    "native": "Deutsch",
    "global": "German"
    }
];

@Injectable()
export class LanguageService {
  private _environmentProperties: IApplicationEnvironment;

  // Observable source (e.g. response from server)
  private _languageList = new BehaviorSubject<ILanguage[]>(DEFAULT_LANGUAGES_LIST);

  // Observable stream (e.g. components subscribe to this stream)
  languageList$ = this._languageList.asObservable();

  constructor(private http: HttpClient, private environment: EnvironmentService) {
    // Resolve environment properties
    this._environmentProperties = environment.getEnvironmentProperties();

    // Initialize read
    this.getLanguages();
  }

  // Method for retriving list of languages from server
  getLanguages() {
    this.http
      .get<ILanguage[]>(this._environmentProperties.apiURL + '/api/languages')
      .retry(5)
      .subscribe(
        response => {
          this._languageList.next(response);
        },
        (error: HttpErrorResponse) => {
            console.error("Error occured.", error);
            if (error.error instanceof Error) {
              // A client-side or network error occurred. Handle it accordingly.
              console.log('An error occurred:', error.error.message);
            } else {
              // The backend returned an unsuccessful response code.
              // The response body may contain clues as to what went wrong,
              console.log(`Backend returned code ${error.status}, body was: ${error.error}`);
            }
          }
      );
  }
}
