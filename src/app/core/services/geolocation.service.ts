import { Injectable } from '@angular/core';

import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

import { IGeolocation } from '../core.interfaces';

const LOCATION: IGeolocation = {
      "latitude": 0,
      "longitude": 0
    }

@Injectable()
export class GeolocationService {

  // Observable source (e.g. response from server)
  private _location = new BehaviorSubject<IGeolocation>(LOCATION);

  // Observable stream (e.g. components subscribe to this stream)
  location$ = this._location.asObservable();

  constructor() {
    this.getGeolocation();
  }

  // Method for performing check if required storage is available in current environement.
  checkGeolocationCompliance() {
    const _GEOLOCATION_AVAILABLE = navigator.geolocation ? true : false;

    return _GEOLOCATION_AVAILABLE;
  }

  getGeolocation() {
    const _GEOLOCATION_AVAILABLE = this.checkGeolocationCompliance();

    if (_GEOLOCATION_AVAILABLE === false){
      console.log('Geolocation is not available');
    }
    console.log('Geolocation is available');

    // navigator.geolocation.getCurrentPosition(this.successCB, this.errorCB);

    navigator.geolocation.getCurrentPosition(
      (position) => {
        // Success
        const _LATITUDE = position.coords.latitude;
        const _LONGITUDE = position.coords.longitude;

        const LOCATION: IGeolocation = {
          "latitude": position.coords.latitude,
          "longitude": position.coords.longitude
        }
        console.log(':: LOCATION ::', LOCATION);
        this._location.next(LOCATION);
      },
      () => {
        // Error
        console.error('Unable to resolve your location!');
      }
    );
  }

  /* successCB(position) {

    const _GEOLOCATION_AVAILABLE = navigator.geolocation ? true : false;

    if (_GEOLOCATION_AVAILABLE === false) {
      return 'Geolocation not supported.';
    }

    const _LATITUDE = position.coords.latitude;
    const _LONGITUDE = position.coords.longitude;

    const LOCATION: IGeolocation = {
      "latitude": position.coords.latitude,
      "longitude": position.coords.longitude
    }
    console.log(':: LOCATION ::', LOCATION);
    console.log(':: this._location ::', this._location);
    //this._location.next(LOCATION);
  } */

  /* errorCB() {
    console.error('Unable to resolve your location!');
  } */
}
