import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

import 'rxjs/add/operator/retry';

import { EnvironmentService } from '../services/environment.service';
import { StorageService } from './storage.service';
import { IApplicationEnvironment, ITranslation } from '../core.interfaces';

@Injectable()
export class TranslationService {
  private _environmentProperties: IApplicationEnvironment;
  private _selectedLanguage: string

  // Observable source (e.g. response from server)
  private _translationList = new BehaviorSubject<ITranslation[]>([]);

  // Observable stream (e.g. components subscribe to this stream)
  translationList$ = this._translationList.asObservable();

  constructor(private http: HttpClient, private environment: EnvironmentService, browserStorage: StorageService) {
    // Resolve environment properties
    this._environmentProperties = environment.getEnvironmentProperties();
    console.log(':: this._environmentProperties ::', this._environmentProperties);

    // Resolve current language
    this._selectedLanguage = browserStorage.localStorageRetriveItem('languageCode');
  }

  // Method for getting translation locale from API
  getLocaleTranslation(locale: string) {

    this.http
      .get<ITranslation[]>(this._environmentProperties.apiURL + '/api/locale/' + locale)
      //.retry(5)
      .subscribe(
        response => {
          // console.log(':: response ::',response);
          this._translationList.next(response);
        },
        (error: HttpErrorResponse) => {

            if (error.error instanceof Error) {
              // A client-side or network error occurred. Handle it accordingly.
              console.error('An error occurred:', error.error.message);
            } else {
              // The backend returned an unsuccessful response code.
              // The response body may contain clues as to what went wrong,
              console.error(`Backend returned code ${error.status}, body was: ${error.error}`);
            }
          }
        );
  }
}
