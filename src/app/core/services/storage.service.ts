import { Injectable } from '@angular/core';

import { ILocalStorage } from '../core.interfaces';

@Injectable()
export class StorageService {

  constructor() {

  }

  // Method for performing check if required storage is available in current environement.
  checkStorageCompliance(requestedStorage: string) {
    let _storageAvailable: boolean;
    if (requestedStorage === 'LOCAL') {
      // Check if exists in window object
      _storageAvailable = window.localStorage ? true : false;

      return _storageAvailable;
    }
    else if (requestedStorage === 'SESSION') {
      // Check if exists in window object
      _storageAvailable = window.sessionStorage ? true : false;

      return _storageAvailable;
    }
    else {
      console.error(':: WARNING :: Invalid storage type was requested in method checkStorageCompliance. Expected value is either LOCAL or SESSION, however ' + requestedStorage + ' was recived.');

      return false;
    }
  }

  // Method for storing data into browser local storage
  localStorageSaveItem(key: string, value: any) {
    // Check if storage is available
    const _STORAGE_AVAILABLE = this.checkStorageCompliance('LOCAL');

    if (_STORAGE_AVAILABLE === true) {
      window.localStorage.setItem(key, value);

      return true;
    }
    else {
      console.error(':: WARNING :: Method localStorageSaveItem was not able to deterime if local storage is available.');
    }
  }

  // Method for storing data into browser session storage
  sessionStorageSaveItem(key: string, value: any) {
    // Check if storage is available
    const _STORAGE_AVAILABLE = this.checkStorageCompliance('SESSION');

    if (_STORAGE_AVAILABLE === true) {
      window.sessionStorage.setItem(key, value);

      return true;
    }
    else {
      console.error(':: WARNING :: Method sessionStorageSaveItem was not able to determine if session storage is available.');
    }
  }

  // Method for retriving data from browser local storage
  localStorageRetriveItem(key: string) {
    // Check if storage is available
    const _STORAGE_AVAILABLE = this.checkStorageCompliance('LOCAL');

    if (_STORAGE_AVAILABLE === true) {
      const ITEM = window.localStorage.getItem(key);

      return ITEM;
    }
    else {
      console.error(':: WARNING :: Method localStorageRetriveItem was not able to determine if local storage is available.');
    }
  }

  // Method for retriving data from browser local storage
  sessionStorageRetriveItem(key: string) {
    // Check if storage is available
    const _STORAGE_AVAILABLE = this.checkStorageCompliance('SESSION');

    if (_STORAGE_AVAILABLE === true) {
       const ITEM = window.sessionStorage.getItem(key);

       return ITEM;
    }
    else {
      console.error(':: WARNING :: Method sessionStorageRetriveItem was not able to determine if session storage is available.');
    }
  }
}
