import { Component, OnInit, OnDestroy, forwardRef, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR, NG_VALIDATORS } from '@angular/forms';
import { Subscription } from 'rxjs/Subscription';

import { TranslationService } from '../../core/services/translation.service';
import { IResetPassword } from '../../core/core.interfaces';

export const YB_RESET_PASSWORD_VALUE_ACCESSOR: any = {
  provide: NG_VALUE_ACCESSOR,
  useExisting: forwardRef(() => ResetPasswordComponent),
  multi: true
}

@Component({
  selector: 'yb-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.scss'],
  providers: [
    YB_RESET_PASSWORD_VALUE_ACCESSOR
  ]
})
export class ResetPasswordComponent implements OnInit, OnDestroy, ControlValueAccessor {
  // The method set in registerOnChange, it is just a placeholder for a method that takes one parameter,
  // we use it to emit changes back to the form
  private _controlValueAccessorChangeFn: (value: any) => void = () => {

  };

  private _password: string = '';
  private _confirmPassword: string = '';

  public get password(): string {
    return this._password;
  }

  public set password(password: string) {
    if (password.length > 0) {
      this._password = password;

      // Propagate change when setting new value
      this._controlValueAccessorChangeFn(this._password)
    }
  }

  public get confirmPassword(): string {
    return this._confirmPassword;
  }

  public set confirmPassword(confirmPassword: string) {
    if (confirmPassword.length > 0) {
      this._confirmPassword = confirmPassword;

      // Propagare change when setting new value
      this._controlValueAccessorChangeFn(this._confirmPassword);
    }
  }


  // Error state
  errorMessage: string;

  // Multilanguage properties
  password_CAPTION: string = '';
  confirmPassword_CAPTION: string = '';

  constructor(private translationService: TranslationService, private changeDetectorRef: ChangeDetectorRef) {

  }

  ngOnInit() {
    // Subscribe to the localization
    this.translationService.translationList$
      .subscribe(
        translation => {
          // console.log(":: translation ::", translation);

          // If translationService throws an error the stream still emits. Thus it is imperative to check
          // if length of the array is greater then 0.
          if (translation.length > 0) {
            this.password_CAPTION = translation[6].value;
            this.confirmPassword_CAPTION = translation[7].value;
          }
          // console.log('::  this.password_CAPTION ::',  this.password_CAPTION);
          // console.log('::  this.confirmPassword_CAPTION ::',  this.confirmPassword_CAPTION);
        },
        error => {
          console.error(':: ERROR ::', error);
          this.errorMessage = <any>error;
        }
      )
  }

  ngOnDestroy() {

  }

  onPasswordChange(event) {
    console.log(':: PASSWORD CHANGED ::', event);
  }

  // ControlValueAccessor interface methods are next. ControlValueAccessor interface will provide us with methods
  // that will interface operations for our control to write values to the native browser DOM. Angular comes with a standard provider for handling form controls called DefaultValueAccessor
  // that works for Inputs, Checkboxes and standard browser elements, we need to add our own implementation for custom controls.
  // Four methods are needed to satisfy this interface:
  // - writeValue
  // - registerOnChange
  // - registerOnTouched
  // - setDisabledState

  // Write a new value to the element (form control). This is the initial value set to the component.
  public writeValue(passwordObject: any): void {
    console.log(':: writeValue() method ::');
  }

  // Registers 'fn' that will be fired when changes are made this is how we emit the changes back to the form.
  public registerOnChange(fn: any): void {
    this._controlValueAccessorChangeFn = fn;
    this.changeDetectorRef.detectChanges();
  }

  // Used for touched input,
  public registerOnTouched(fn: any) {

  }

  // Component internal change event handler.
  private onChange(event) {

  }

  // Set disabled state to the component
  setDisabledState?(isDisabled: boolean): void {

  }


}
