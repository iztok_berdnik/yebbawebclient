import { Component, OnInit, OnDestroy, Input, Output, EventEmitter, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { AbstractControl, FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';

import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';

import { StorageService } from '../../core/services/storage.service';
import { LanguageService } from '../../core/services/language.service';
import { TranslationService } from '../../core/services/translation.service';
import { HelperService } from '../../core/services/helper.service';
import { ILanguage } from '../../core/core.interfaces';


@Component({
  selector: 'yb-language-selection-form',
  templateUrl: './language-selection-form.component.html',
  styleUrls: ['./language-selection-form.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class LanguageSelectionFormComponent implements OnInit {

  // Output
  // @Output() selectedLanguageValue: EventEmitter<any> = new EventEmitter<any>();

  // Input(s)
  // Observable source (e.g. from service)
  // @Input() languageList = new Subject<ILanguage[]>();
  @Input() languageList: ILanguage[] = [];

  // Output(s)
  // TODO :: Rewrite this to be a type of subject
  @Output() selectedLanguage: EventEmitter<ILanguage> = new EventEmitter();

  // Error state
  errorMessage: string;

  //  Form object
  languageSelectionForm: FormGroup;

  // Multilanguage properties
  languageField_CAPTION: string = '';

  constructor(private languageService: LanguageService, private translationService: TranslationService,
    private browserStorage: StorageService, private FormBuilder: FormBuilder, private helper: HelperService,
    private changeDetectorRef: ChangeDetectorRef) {

  }

  ngOnInit() {
    // Subscribe to the source of list of languages
    this.languageService.languageList$
      .subscribe(
        languages => {
          // If translationService throws an error the stream still emits. Thus it is imperative to check
          // if length of the array is greater then 0.
          if (languages.length > 0) {
            this.languageList = languages;
          }
        },
        error => {
          console.error(':: ERROR ::', error);
          this.errorMessage = <any>error;
        }
      )

    // Subscribe to the localization
    this.translationService.translationList$
      .subscribe(
        translation => {

          // If translationService throws an error the stream still emits. Thus it is imperative to check
          // if length of the array is greater then 0.
          if (translation.length > 0) {
            this.languageField_CAPTION = translation[2].value;
          }
        },
        error => {
          console.error(':: ERROR ::', error);
          this.errorMessage = <any>error;
        }
      )

    // Initialize form generation
    this.createForm();

    // Initialize change event helper
    this.formChangeMonitor();

    // Retrive language that is set in the browser local storage
    const BROWSER_LANGUAGE = this.browserStorage.localStorageRetriveItem('languageCode');

    // Set default value
    this.setInitialValue('selectedLanguage', BROWSER_LANGUAGE);
  }

  ngOnDestroy() {
    // Unsubscribe from all observables

  }

  // Method for creating authentication form model.
  createForm() {
    // console.log(":: createForm() metod inLanguageValue ::", this.inLanguageValue);

    this.languageSelectionForm = this.FormBuilder.group({
      selectedLanguage: ''
    });
  }

  setInitialValue(field, value) {
    // const _ITEM = this.findItem(this.languageList, 'code', value);
    const _ITEM = this.helper.findItemInArray(this.languageList, 'code', value);

    // This is correct but it triggers before the languages list is provided by service
    // !!! IMPORTANT !!! This is currently not working because of:
    // https://github.com/angular/material2/issues/2785
    // https://github.com/angular/material2/pull/4540
    this.languageSelectionForm.controls['selectedLanguage'].setValue(_ITEM, {
      onlySelf: true,
      emitEvent: true
    });

    // Mark for change
    this.changeDetectorRef.markForCheck();
  }

  // Method responsible for subscribing to change event observable and passing events to HANDLER
  formChangeMonitor() {
    // Subscribe to validation status changes
    /*this.languageSelectionForm.valueChanges
      .subscribe(data => this.formChangeHandler(data));*/
    this.languageSelectionForm.get('selectedLanguage').valueChanges
      .subscribe(data => {

        // Check if data is object. There is some unusual behavior in Firefox where initial change doesn't contain data ?!.
        // Instead data is undefined
        if (typeof data !== 'object') {
          console.warn(':: WARNING :: Method formChangeMonitor(). Argument data is not an object.');
        }
        else {
          // Update browser storage with new value
          this.browserStorage.localStorageSaveItem('languageCode', data.code);

          // Set new value for lang
          document.documentElement.lang = data.code.substring(0, data.code.indexOf('-'));

          // Trigger request to recive new locale translation when user
          // changes the value of form.
          if (this.languageSelectionForm.dirty === true) {
            this.translationService.getLocaleTranslation(data.code);
          }

          // Emit event
          this.selectedLanguage.emit(data);
        }
      });
  }
}
