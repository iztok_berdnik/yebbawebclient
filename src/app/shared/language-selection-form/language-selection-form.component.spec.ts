import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LanguageSelectionFormComponent } from './language-selection-form.component';

describe('LanguageSelectionFormComponent', () => {
  let component: LanguageSelectionFormComponent;
  let fixture: ComponentFixture<LanguageSelectionFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LanguageSelectionFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LanguageSelectionFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
