import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MdSelectModule } from '@angular/material';
import { FlexLayoutModule } from '@angular/flex-layout';

import { LanguageSelectionFormComponent } from './language-selection-form.component';
import { LanguageService } from '../../core/services/language.service';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    MdSelectModule,
    FlexLayoutModule
  ],
  declarations: [
    LanguageSelectionFormComponent
  ],
  exports: [
    LanguageSelectionFormComponent
  ],
  providers: [
    LanguageService
  ]
})
export class LanguageSelectionFormModule {

}
