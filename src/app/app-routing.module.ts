import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const APP_ROUTES: Routes = [
    {
        path: 'authentication',
        loadChildren: './authentication/authentication.module#AuthenticationModule'
    },
    /*{
        path: 'home',
        loadChildren: './home-page/home-page.module#HomePageModule'
    },*/
    {
        path: '',
        redirectTo: '/authentication',
        pathMatch: 'full'
    },
    {
        path: '**',
        redirectTo: '/authentication',
        pathMatch: 'full'
    }
];

@NgModule({
    imports: [RouterModule.forRoot(APP_ROUTES, {
       // enableTracing: true
    })
    ],
    exports: [
        RouterModule
    ],
  providers: []
})
export class AppRoutingModule {

}
