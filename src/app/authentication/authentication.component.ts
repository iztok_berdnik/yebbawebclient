import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';

import { LanguageSelectionFormComponent } from '../shared/language-selection-form/language-selection-form.component';
import { TranslationService } from '../core/services/translation.service';

@Component({
  selector: 'yb-authentication',
  templateUrl: './authentication.component.html',
  styleUrls: ['./authentication.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AuthenticationComponent implements OnInit {

  // Error state
  errorMessage: string;

  // Logo
  autheticationLogo: any = null;

  // Multilanguage properties
  copyright_CAPTION: string = '';
  authenticationCompany_CAPTION: string = '';
  disclaimer_CAPTION: string = '';
  rightsDisclaimer_CAPTION: string;

  constructor(private translationService: TranslationService) {
    // this.getLocalization();

    // Subscribe to the localization
    this.translationService.translationList$
      .subscribe(
        translation => {

          // If translationService throws an error the stream still emits. Thus it is imperative to check
          // if length of the array is greater then 0.
          if (translation.length > 0) {
            this.copyright_CAPTION = translation[0].value;
            this.rightsDisclaimer_CAPTION = translation[1].value;
          }
        },
        (error: any) => {
          console.error("Error occured.", error);
          if (error.error instanceof Error) {
            // A client-side or network error occurred. Handle it accordingly.
            console.log('An error occurred:', error.error.message);
          } else {
            // The backend returned an unsuccessful response code.
            // The response body may contain clues as to what went wrong,
            console.log(`Backend returned code ${error.status}, body was: ${error.error}`);
          }
        }
      )
  }

  ngOnInit() {
    // Set URL for authentication cover image
    this.autheticationLogo = '../../assets/images/yebba.svg';

    this.authenticationCompany_CAPTION = 'yebba '

    // Determine current year
    const _today = new Date();
    const _year = _today.getFullYear();

    this.disclaimer_CAPTION = _year + ' :: ';
  }
}
