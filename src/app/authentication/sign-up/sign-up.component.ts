import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';


import { Observable } from 'rxjs/Observable';

import { TranslationService } from '../../core/services/translation.service';
import { GeolocationService } from '../../core/services/geolocation.service';
import { IGeolocation } from '../../core/core.interfaces';


@Component({
  selector: 'yb-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.scss']
})
export class SignUpComponent implements OnInit {

  // Geolocation
  geolocationObject: IGeolocation = null;

  // Multilanguage properties
  formTitle_CAPTION: string = '';
  formSubtitle_CAPTION: string = '';

  constructor(private translationService: TranslationService, private geolocation: GeolocationService) {


  }

  ngOnInit() {
    // Subscribe to the localization
    this.translationService.translationList$
      .subscribe(
        translation => {

          // If translationService throws an error the stream still emits. Thus it is imperative to check
          // if length of the array is greater then 0.
          if (translation.length > 0) {
            this.formTitle_CAPTION = translation[4].value;
            this.formSubtitle_CAPTION = translation[5].value;
          }
        },
        error => {
          console.error(':: ERROR ::', error);
          this.formTitle_CAPTION = <any>error;
        }
      )

    // Subscribe to the geolocation
    this.geolocation.location$
      .subscribe(
        location => {
          console.log(':: location ::', location);
          this.geolocationObject = location;
        },
        error => {
          console.error(':: ERROR ::', error);
          this.formTitle_CAPTION = <any>error;
        }
      );
  }

}
