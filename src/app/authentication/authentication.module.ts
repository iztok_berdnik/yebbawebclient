import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FlexLayoutModule } from '@angular/flex-layout';

import { AuthenticationRoutingModule } from './authentication-routing.module';
import { LanguageSelectionFormModule } from '../shared/language-selection-form/language-selection-form.module';
import { ResetPasswordModule } from '../shared/reset-password/reset-password.module';
import { AuthenticationComponent } from './authentication.component';
import { SignUpComponent } from './sign-up/sign-up.component';
import { ResetCredentialsComponent } from './reset-credentials/reset-credentials.component';

@NgModule({
  imports: [
    CommonModule,
    FlexLayoutModule,
    AuthenticationRoutingModule,
    LanguageSelectionFormModule,
    ResetPasswordModule
  ],
  declarations: [
    AuthenticationComponent,
    SignUpComponent,
    ResetCredentialsComponent
  ],
  exports: [
    AuthenticationComponent,
    SignUpComponent
  ],
  providers: [
  ]
})
export class AuthenticationModule { }
