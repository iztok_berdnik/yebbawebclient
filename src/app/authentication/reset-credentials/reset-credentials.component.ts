import { Component, OnInit } from '@angular/core';

import { Observable } from 'rxjs/Observable';

import { TranslationService } from '../../core/services/translation.service';
import { ResetPasswordComponent } from '../../shared/reset-password/reset-password.component';

@Component({
  selector: 'yb-reset-credentials',
  templateUrl: './reset-credentials.component.html',
  styleUrls: ['./reset-credentials.component.scss']
})
export class ResetCredentialsComponent implements OnInit {

   // Multilanguage properties
  formTitle_CAPTION: string = '';
  formSubtitle_CAPTION: string = '';

  constructor(private translationService: TranslationService) { }

  ngOnInit() {
    // Subscribe to the localization
    this.translationService.translationList$
      .subscribe(
        translation => {

          // If translationService throws an error the stream still emits. Thus it is imperative to check
          // if length of the array is greater then 0.
          if (translation.length > 0) {
            this.formTitle_CAPTION = translation[8].value;
            this.formSubtitle_CAPTION = translation[5].value;
          }
        },
        error => {
          console.error(':: ERROR ::', error);
          this.formTitle_CAPTION = <any>error;
        }
      )
  }

}
