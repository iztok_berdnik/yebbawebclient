import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AuthenticationComponent } from './authentication.component';
import { SignUpComponent } from './sign-up/sign-up.component';
import { ResetCredentialsComponent } from './reset-credentials/reset-credentials.component';

const routes: Routes = [
  {
    path: '',
    component: AuthenticationComponent,
    children: [
      {
        path: 'signup',
        component: SignUpComponent
      },
      {
        path: 'reset',
        component: ResetCredentialsComponent,
      }
      /*
      {
        path: 'forgotCredentials',
        component: ForgotCredentialsComponent
      },
      {
        path: 'registerUser',
        component: RegisterNewUserComponent
      },
      {
        path: '',
        redirectTo: 'authenticate',
        pathMatch: 'full'
      }
      */
    ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AuthenticationRoutingModule { }
