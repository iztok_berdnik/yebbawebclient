import { InMemoryBackendConfig, InMemoryDbService } from 'angular-in-memory-web-api';

import { ILanguage } from './interfaces';

export class LanguageData implements InMemoryDbService, InMemoryBackendConfig {
  createDb() {
    let languagesMock: ILanguage[] = [
       {
        'id': 1,
        'code': 'en-US',
        'native': 'English',
        'global': 'English'
      },
      {
        'id': 2,
        'code': 'sl-SI',
        'native': 'Slovenščina',
        'global': 'Slovenian'
      },
      {
        'id': 3,
        'code': 'de-DE',
        'native': 'Deutsch',
        'global': 'German'
      }
    ];
    // Return object
    return { languagesMock };
  }
}

