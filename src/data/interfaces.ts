/* LANGUAGE */
export interface ILanguage {
  id: number;
  code: string;
  native: string;
  global: string;
}

/* TRANSLATION */
export interface ITranslation {
  id: number;
  value: string;
}
