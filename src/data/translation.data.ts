import { InMemoryBackendConfig, InMemoryDbService } from 'angular-in-memory-web-api';

import { ITranslation } from './interfaces';

export class translationsData implements InMemoryDbService, InMemoryBackendConfig {

  constructor(private translation_EN: ITranslation, private TRANSLATION_SI: ITranslation, private selectedLanguage: string) {

  }

  createDb() {
    let translationMock: any[];

    if (this.selectedLanguage == 'en') {
      translationMock = [
        {
          "id": 1,
          "value": "Copyright yebba ©"
        },
        {
          "id": 2,
          "value": "All rights reserved"
        }
      ];
    }
    else if (this.selectedLanguage == 'sl') {
      translationMock = [
        {
          "id": 1,
          "value": "Avtorske pravice yebba ©"
        },
        {
          "id": 2,
          "value": "Vse pravice pridržane"
        }
      ];
    }
    else if (this.selectedLanguage == 'de') {
      translationMock = [
        {
          "id": 1,
          "value": "Copyright yebba ©"
        },
        {
          "id": 2,
          "value": "Alle Rechte vorbehalten"
        }
      ];
    }
    else {
      translationMock = [
        {
          "id": 1,
          "value": "Copyright yebba ©"
        },
        {
          "id": 2,
          "value": "All rights reserved"
        }
      ];
    }
    // Return object
    return { translationMock };
  }
}
