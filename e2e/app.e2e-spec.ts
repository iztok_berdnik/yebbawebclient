import { YebbaPage } from './app.po';

describe('yebba App', () => {
  let page: YebbaPage;

  beforeEach(() => {
    page = new YebbaPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
